#!/bin/bash

readonly traps=SIGINT SIGQUIT SIGTERM EXIT 
readonly opacity=$(awk '/^background_opacity/ { print $2 }' $HOME/.config/kitty/kitty.conf)

function trap_with() {
    # trap_with_arg <handler> <pid> <signals>...
    # handler <signal> <pid>
    local -r handler="$1"
    shift 
    for sig in "$@"; do
        trap "${handler} ${sig}" "$sig"
    done
}

function cleanup() {
    local -r sig="$1"
    kitty @set-background-opacity $opacity
    trap - "$sig"
    kill -s "$sig" "$$" 
}

if [[ "$TERM" =~ kitty ]]; then
    kitty @set-background-opacity 1
    trap_with cleanup $traps
    "$@"
    kitty @set-background-opacity $opacity
else
    exec zsh -c "$@"
fi
