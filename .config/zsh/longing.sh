#!/usr/bin/env bash

when=$(date '+%s%N' -d '12/11 3:25PM')
now=$(date '+%s%N')
declare -ir when now

#shellcheck disable=SC2034
declare -ar units=(week day hour minute second)
declare -ar until=(
    "i see you"
    "i kiss you"
    "i see my beautiful girlfriend"
    "i fall into your arms"
    "i embarrass myself in an airport"
    "i hold your hand"
    "we fall asleep holding each other"
    "you take me home"
    "i lean my head on your shoulder"
    "i hear your voice"
    "i taste you"
    "i run my fingers through your hair"
)

function pluralize() {
    #usage: pluralize <values> <words> <output>
    local -nr values=$1
    local -nr words=$2
    local -n output=$3
    for i in "${!values[@]}"; do
        case ${values[i]} in
            0) continue ;;
            1) output+=("${values[i]} ${words[i]}") ;;
            *) output+=("${values[i]} ${words[i]}s") ;;
        esac
    done
}

function pretty_list() {
    #usage: pretty_list <value>...
    printf '%s, ' "${@:1:$#-1}"
    echo "and" "${@: -1}"
}

function time_until_i_see_my_gf() {
    local -ir total_seconds=$(( (when - now) / 10 ** 9))
    local -ir total_minutes=$(( total_seconds / 60 ))
    local -ir total_hours=$(( total_minutes / 60 ))
    local -ir total_days=$(( total_hours / 24 ))

    local -ir seconds=$(( total_seconds % 60 ))
    local -ir minutes=$(( total_minutes % 60 ))
    local -ir hours=$(( total_hours % 24 ))
    local -ir days=$(( total_days % 7 ))
    local -ir weeks=$(( total_days / 7 ))

    #shellcheck disable=SC2034
    local -ar times=("$weeks" "$days" "$hours" "$minutes" "$seconds")
    local -a fmt_times
    pluralize times units fmt_times
    pretty_list "${fmt_times[@]}"
}

if (( when > now )); then
    echo "$(time_until_i_see_my_gf)

    until ${until[RANDOM % ( ${#until[@]} - 1 ) ]}"
else
    echo "i love her."
fi
