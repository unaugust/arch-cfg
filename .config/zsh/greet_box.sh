#!/usr/bin/env bash

readonly -a styles=(
    thick
    thin
    double
    rounded
    dot
)

readonly display="${styles[RANDOM % ${#styles[@]} - 1]}"


while read -r line; do echo "$line"; done \
    | little_boxes -c "$display" \
    | pridecat --lesbian -l
