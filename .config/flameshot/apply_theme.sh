#!/bin/sh
# flameshot config gui reformats flameshot.ini so we're seding it up

# Start flavours
# Base16 lavender ROFI Color theme
#
# Authors
#  Scheme: August Lounibos
#  Template: August Lounibos
contrastUiColor=#312b59
drawColor=#c748ed
uiColor=#9f4cf5
# End flavours

config="${HOME}/.config/flameshot/flameshot.ini"

sed -i "$config" \
    -e "s/^contrastUiColor=.*$/contrastUiColor=${contrastUiColor}/"\
    -e "s/^drawColor=.*$/drawColor=${drawColor}/"\
    -e "s/^uiColor=.*$/uiColor=${uiColor}/"
