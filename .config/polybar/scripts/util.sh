#!/bin/bash

min() {
    local -ri a=$1
    local -ri b=$2
    echo $(( a > b ? b : a ))
}

max() {
    local -ri a=$1
    local -ri b=$2
    echo $(( a < b ? b : a ))
}

log() {
    printf "%s\n" "$*" >&2
}

ramp() {
    # $1: number [0,100]
    # ${@:1} emojis
    local -ri n=$1
    shift
    local -i index=$(( (n / (100 / $#)) ))
    local -ra ramp=( "$@" "${@: -1}")
    echo "${ramp[index]}"
}

idle() {
    # $1 format string
    # $2 animation array nameref
    # shellcheck disable=SC2034 # linter doesn't understand indirection
    local -ar default_ramp=(⠤ ⢂ ⡐)
    local -r format=${1:-'%s'}
    local -n idle__ramp=${2:-"default_ramp"}
    local -i i
    for (( i=0; ; i++ )); do
        # shellcheck disable=SC2059 # intentionally a format string
        printf "$format" "${idle__ramp[ i % ${#idle__ramp[@]} ]}"
        sleep 0.1
    done
}

rep() {
    local -i i
    for (( i = 0; i < $2; i++ )); do
        echo -n "$1"
    done
    echo
}

progress_bar() {
    local -ir progress=$1
    local -ir width=$2
    local -r alert_color="#${3:-"ff0000"}"

    local -ir bar_chars=$(( progress * width / 100 ))
    local -ir over_bar_chars=$(( bar_chars - width ))
    local -ir full_bar_chars=$(( bar_chars > width ? 2 * width - bar_chars : bar_chars ))
    local -r over_bar=$(rep ━ $over_bar_chars)
    local -r full_bar=$(rep ━ $full_bar_chars)
    local -r empty_bar=$(rep ' ' $(( width - bar_chars )) )
    echo "<span foreground=\"${alert_color}\">${over_bar}</span>${full_bar}${empty_bar}"
}

array_diff() {
    local -n array_diff__a1=$1
    local -n array_diff__a2=$2
    local -n diff=$3
    diff=()
    for key in "${!array_diff__a1[@]}"; do
        if [[ "${array_diff__a1[$key]}" != "${array_diff__a2[$key]}" ]]; then
            diff+=("$key")
        fi
    done
}

array_copy() {
    local -n array_copy__a1=$1
    local -n array_copy__a2=$2

    for key in "${!array_copy__a1[@]}"; do
        # shellcheck disable=SC2034 # linter doesn't understand indirection
        array_copy__a2["$key"]="${array_copy__a1[$key]}"
    done
}

array_contains() {
    local -n array_contains__array=$1
    for e in "${array_contains__array[@]}"; do
        [[ "$e" ==  "$2" ]] && return 0
    done
    return 1
}

