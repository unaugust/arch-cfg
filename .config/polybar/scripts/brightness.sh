#!/bin/bash

source "$(dirname "${BASH_SOURCE[0]}")"/util.sh

COLOR="#${COLOR:-888}"
ICON_LIGHT=${ICON_LIGHT:-"light"}
read -ra icon_light_ramp <<< "$ICON_LIGHT"
if [[ -n "$ICON_DUNST_LIGHT" ]]; then
    readonly -a dunst_icon_flag=( "-I" "${ICON_DUNST_LIGHT/#\~/$HOME}" )
fi
readonly hw_file=/sys/class/backlight/intel_backlight/actual_brightness

update_status() {
    local -ri level=$1
    local -r icon=$(ramp "$level" "${icon_light_ramp[@]}")
    local -r vol_fmt=$(printf "%02d%%" "$level")
    echo "%{F$COLOR}${icon}%{F-} %{T2}$vol_fmt"
}

notify() {
    local -ri level=$1
    local -r message="$(progress_bar "$level" 30)"
    dunstify -r 1312 "${dunst_icon_flag[@]}" ":simple-monospace:" "$message"
}

get_brightness() {
    printf "%.0f" "$(light)"
}

show() {
    local -i level
    level="$(get_brightness)"
    update_status "$level"
    inotifywait -qm -e "modify" "$hw_file" | while read -r; do
        level="$(get_brightness)"
        update_status "$level"
        notify "$level"
    done
}

up() {
    light -A 5
}

down() {
    light -U 5
}

usage() {
    echo "usage: brightness.sh [--show|--up|--down]"
}

case "$1" in
    --show) show  ;;
    --up)   up    ;;
    --down) down  ;;
    *)      usage ;;
esac
