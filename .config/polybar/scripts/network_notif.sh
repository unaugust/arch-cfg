#!/bin/bash

readonly icon=~/.config/polybar/scripts/icons/wifi.png
readonly message="$1"
dunstify -r 1312 -I "$icon" ":simple:" "$message"
