#!/bin/bash

source "$(dirname "${BASH_SOURCE[0]}")"/util.sh

ALERT=${ALERT:-900}
COLOR=${COLOR:-d88}

ICON_HEADPHONES_MUTED=${ICON_HEADPHONES_MUTED:-"muted"}
ICON_HEADPHONES_VOLUME=${ICON_HEADPHONES_VOLUME:-"volume"}
ICON_MUTED=${ICON_MUTED:-"muted"}
ICON_VOLUME=${ICON_VOLUME:-"volume"}

read -ra icon_volume_ramp <<< "$ICON_VOLUME"
read -ra icon_headphones_volume_ramp <<< "$ICON_HEADPHONES_VOLUME"
readonly icon_volume_ramp icon_headphones_volume_ramp

get_state_change() {
    local -a state_diff
    array_diff "$1" "$2" state_diff
    if array_contains state_diff headphones; then
        echo headphones
        return
    fi
    echo "${state_diff[0]}"
}

get_state() {
    local -n get_state__state=$1
    # shellcheck disable=SC2034    # shcheck doesn't understand references
    get_state__state=(
        ['headphones']="$(
            ! pactl list sinks | grep -q "Active Port: .*headphones"; echo $?)"
        ['volume']=$(pulsemixer --get-volume | cut -d ' ' -f 1)
        ['mute']=$(pulsemixer --get-mute)
    )
}

update_status() {
    local -n update_status__state=$1
    local -ri is_headphones=${update_status__state['headphones']}
    local -ri is_muted=${update_status__state['mute']}
    local -ri volume=${update_status__state['volume']}

    local -r icon="$(case "$is_headphones $is_muted" in
        ("1 1") echo "$ICON_HEADPHONES_MUTED"                                   ;;
        ("1 0") ramp "$(min 100 "$volume")" "${icon_headphones_volume_ramp[@]}" ;;
        ("0 1") echo "$ICON_MUTED"                                              ;;
        ("0 0") ramp "$(min 100 "$volume")" "${icon_volume_ramp[@]}"            ;;
    esac)"
    local -r vol_fmt="$(case "$is_muted" in
        (0) printf "%02d%%\n" "$volume"                                         ;;
        (1) echo '--%'                                                          ;;
    esac)"

    echo "%{F#${COLOR}}${icon}%{F-} %{T2}$vol_fmt"
}

notify() {
    local -n notify__state=$1
    local -r title="$(case "$2" in
        (volume) echo ":simple-monospace:"                              ;;
        (*) echo ":simple:"                                             ;;
    esac)"
    local -r body="$(case "$2 ${notify__state['mute']} ${notify__state['headphones']}" in
        (volume*) progress_bar "${notify__state['volume']}" 30 "$ALERT" ;;
        ("mute 1"*) echo "Volume Muted"                                 ;;
        ("mute 0"*) echo "Volume Unmuted"                               ;;
        (headphones\ ?\ 1) echo "Speakers Connected"                    ;;
        (headphones\ ?\ 0) echo "Speakers Disconnected"                 ;;
    esac)"
    local -r icon="$(case "$2 ${notify__state['mute']}" in
        ("mute 0"|volume*) echo "$ICON_DUNST_VOLUME"                    ;;
        ("mute 1") echo "$ICON_DUNST_MUTED"                             ;;
        (headphones*) echo "$ICON_DUNST_HEADPHONES"                     ;;
    esac)"

    if [[ -n "$icon" ]]; then
        local -ra icon_flag=( "-I" "${icon/#\~/"$HOME"}" )
    fi
    dunstify -r 1312 "${icon_flag[@]}" "$title" "$body"
}

show() {
    # shellcheck disable=SC2034    # shcheck doesn't understand indirection
    local -Ai audio_state new_state
    local change
    local -r re="sink #"

    get_state audio_state
    update_status audio_state
    pactl subscribe | while read -r; do
        if [[ "$REPLY" =~ $re ]]; then
            get_state new_state
            change=$(get_state_change audio_state new_state)
            if [[ "$change" ]]; then
                array_copy new_state audio_state
                update_status audio_state
                notify audio_state "$change"
            fi
        fi
    done
}

down() {
    pulsemixer --change-volume -5
}

up() {
    pulsemixer --change-volume +5
}

mute() {
    pulsemixer --toggle-mute
}

usage() {
    echo "usage: volume.sh [--show|--mute|--up|--down]"
}

case "$1" in
    --show)   show   ;;
    --up)     up     ;;
    --down)   down   ;;
    --mute)   mute   ;;
    *)        usage  ;;
esac
