#!/bin/bash

declare -i sleep_pid=0
#readonly dunst_icon_off=~/.config/polybar/scripts/icons/bell-off.png
#readonly dunst_icon_on=~/.config/polybar/scripts/icons/bell-on.png
COLOR="#${COLOR:-88d}"
ICON_BELL_ON=${ICON_BELL_ON:-"on"}
ICON_BELL_OFF=${ICON_BELL_OFF:-"off"}

toggle() {
    # $1 should be pid of sh process executing the module's exec command (%pid%)
    #dunstify -r 1312 -I "$dunst_icon_off" ":simple:" "Notifications Off"
    dunstctl set-paused toggle
    #dunstify -r 1312 -I "$dunst_icon_on" ":simple:" "Notifications On"
    [[ -z "$1" ]] && exit
    kill -USR1 "$(pgrep --oldest --parent "$1")"
}

toggle_impl() {
    if (( sleep_pid != 0 )); then
        kill $sleep_pid > /dev/null/ 2>&1
    fi
}

trap "toggle_impl" USR1

update_status() {
    if [[ "$(dunstctl is-paused)" == "false" ]]; then
        echo "%{F${COLOR}}${ICON_BELL_ON}%{F-}"
    else
        echo "$ICON_BELL_OFF"
    fi
}

start() {
    while true; do
        update_status
        sleep 10s &
        sleep_pid=$!
        wait
    done
}

usage() {
    echo "usage: checkupdates.sh [--start|--toggle %pid%]"
}

case "$1" in
    --start)    start           ;;
    --toggle)   toggle "$2"     ;;
    *)          usage           ;;
esac
