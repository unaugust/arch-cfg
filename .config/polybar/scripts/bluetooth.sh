#!/bin/bash

source "$(dirname "${BASH_SOURCE[0]}")"/util.sh

HL_COLOR=${HL_COLOR:-88d}
COLOR=${COLOR:-d88}
ICON_BT_OFF=${ICON_BT_OFF:-"bluetooth off"}
ICON_BT_ON=${ICON_BT_ON:-"bluetooth on"}
ICON_BT_PAIRED=${ICON_BT_PAIRED:-"bluetooth paired"}
if [[ -n "$ICON_DUNST" ]]; then
    readonly -a dunst_icon_flag=( "-I" "${ICON_DUNST/#\~/$HOME}" )
fi

connected_devices() {
    bluetoothctl devices | cut -f 2 -d ' ' | while read -r uuid; do
        info=$(bluetoothctl info "$uuid")
        #TODO this is probabyly just a single awk command
        if grep -q -e "Connected: yes" <<< "$info"; then
            grep -e "Alias:" <<< "$info" | cut -f 2- -d ' '
        fi
    done
}

update_status() {
    local -n update_status__state=$1
    if [[ "${update_status__state['connected']}" == "yes" ]]; then
        echo "%{u#${HL_COLOR}}%{+u}%{F#${HL_COLOR}} ${ICON_BT_PAIRED} %{F-}%{u-}"
    elif [[ "${update_status__state['powered']}" == "yes" ]]; then
        echo "%{F#${COLOR}}${ICON_BT_ON}%{F-}"
    else
        echo "${ICON_BT_OFF}"
    fi
}

notify() {
    local -n notify__state=$1
    local -r change=$2
    local -r message="$(case "$change ${notify__state[$change]}" in
        ("powered yes")   echo "Bluetooth Power On";;
        (powered*)        echo "Bluetooth Power Off";;
        ("connected yes") echo "Bluetooth Device Connected";;
        (connected*)      echo "Bluetooth Device Disconnected";;
    esac)"
    dunstify -r 1312 "${dunst_icon_flag[@]}" ":simple:" "$message"
}

parse_info() {
    local -n parse_info__state=$1
    local -r re="(Connected|Powered):[[:blank:]]([[:alpha:]]+)"
    if [[ "$2" =~ $re ]]; then
        # shellcheck disable=SC2034    # shcheck doesn't understand indirection
        parse_info__state["${BASH_REMATCH[1],,}"]="${BASH_REMATCH[2]}"
        return 0
    fi
    return 1
}

show() {
    local -A bt_state=(['connected']=no ['powered']=no)
    # shellcheck disable=SC2034    # shcheck doesn't understand indirection
    local -A new_bt_state
    local -a state_delta
    parse_info bt_state "$(bluetoothctl show)"
    if [[ "$(connected_devices)" ]]; then
        bt_state['connected']=yes
    fi
    array_copy bt_state new_bt_state
    update_status bt_state

    bluetoothctl | while read -r; do
        if parse_info new_bt_state "$REPLY"; then
            array_diff bt_state new_bt_state state_delta
            log "${state_delta[@]}"
            array_copy new_bt_state bt_state
            update_status bt_state
            notify bt_state "${state_delta[0]}"
        fi
    done
}

toggle() {
    local -A bt_state
    parse_info bt_state "$(bluetoothctl show | grep Powered)"
    if [[ "${bt_state['powered']}" == "yes" ]]; then
        bluetoothctl power off
    else
        bluetoothctl power on
    fi
}

usage() {
    echo "usage: bluetooth.sh [--toggle|--show]"
}

case "$1" in
    --toggle) toggle ;;
    --show)   show;;
    *)        usage  ;;
esac
