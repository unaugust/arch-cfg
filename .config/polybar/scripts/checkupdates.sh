#!/bin/bash

source "$(dirname "${BASH_SOURCE[0]}")"/util.sh

INTERVAL=${INTERVAL:-8h}
HL_COLOR="#${HL_COLOR:-88d}"
COLOR="#${COLOR:-d8d}"
ICON_UPDATE=${ICON_UPDATE:-"updates available"}
ICON_NO_UPDATE=${ICON_NO_UPDATE:-"no updates"}
ICON_LOADING=${ICON_LOADING:-"loading..."}
ICON_ALERT=${ICON_ALERT:-"needs restart"}
if [[ -n "$ICON_DUNST_UPDATE" ]]; then
    readonly -a dunst_icon_flag=( "-I" "${ICON_DUNST_UPDATE/#~/$HOME}" )
fi

declare -i sleep_pid=0
readonly watchlist=("discord" "firefox" "flameshot" "linux" "neovim" "polybar")

refresh() {
    # $1 should be pid of sh process executing the module's exec command (%pid%)
    [[ -z "$1" ]] && exit
    kill -USR1 "$(pgrep --oldest --parent "$1")"
}

refresh_impl() {
    if (( sleep_pid )); then
        kill $sleep_pid > /dev/null/ 2>&1
    fi
}

trap "refresh_impl" USR1

needs_restart() {
    [[ $(pacman -Q linux | cut -f 2 -d ' ') > $(uname -r | sed 's/-/./') ]]
    return $?
}

parse_updates() {
    local -n parse_updates__count=$1
    local -n parse_updates__important=$2
    local -r updates_list=$(cat <(paru -Qu) <(checkupdates))
    [[ "$updates_list" ]] || return
    # shellcheck disable=SC2034    # shcheck doesn't understand indirection
    parse_updates__count=$(wc -l <<< "$updates_list")
    # shellcheck disable=SC2034    # shcheck doesn't understand indirection
    parse_updates__important=$(
        sort <(printf '%s\n' "${watchlist[@]}") \
             <(cut -f 1 -d ' ' <<< "$updates_list") \
        | uniq -d \
        | paste -s -d ' ' \
        | sed 's/ /,&/g')
}

notify() {
    local -ir display_count=$(( $1 - $(wc -w <<< "$2") ))
    printf -v message "Updates for %s package%s." \
        "$( (( display_count == $1 )) \
            && printf '%s' "$display_count" \
            || printf '%s' "$2, and $display_count other")" \
        "$( (( display_count == 1 )) \
            || printf '%s' "s")"
    dunstify -r 1312 "${dunst_icon_flag[@]}" ":simple:" "$message"
}

update_status() {
    idle "$ICON_LOADING %%{T5}%s\n" &
    idle_pid=$!
    local -i update_count
    local important_updates
    parse_updates update_count important_updates
    kill $idle_pid
    if (( update_count )); then
        notify "$update_count" "$important_updates"
        if [[ "$important_updates" ]]; then
            echo "%{u$HL_COLOR}%{+u}%{F$HL_COLOR}" \
                 "$ICON_UPDATE $update_count" \
                 "%{F-}%{u-}"
        else
            echo "%{F$COLOR}$ICON_UPDATE%{F-} $update_count"
        fi
    elif needs_restart; then
        dunstify -r 1312 "${dunst_icon_flag[@]}" ":simple:" "Restart required"
        echo "%{F$HL_COLOR}$ICON_ALERT%{F-}"
    else
        echo "$ICON_NO_UPDATE"
    fi
}

start() {
    while true; do
        update_status
        sleep "$INTERVAL" &
        sleep_pid=$!
        wait
    done
}

usage() {
    echo "usage: checkupdates.sh [--start|--refresh %pid%]"
}

case "$1" in
    --start)    start           ;;
    --refresh)  refresh "$2"    ;;
    *)          usage           ;;
esac
