#!/bin/bash

suspend='   suspend'
power_off='   power off'
restart='   restart'
lock='   lock'
tty='   tty'
refresh='   refresh'

if [ "$@" ]; then
    case "$1" in
        "$power_off") 	    poweroff                                             ;;
        "$restart") 	    reboot                                               ;;
        "$suspend") 	    betterlockscreen -s dim > /dev/null 2>&1 < /dev/null ;;
        "$lock") 	        betterlockscreen -l dim > /dev/null 2>&1 < /dev/null ;;
        "$tty")             i3-msg exit                                          ;;
        "$refresh")         i3-msg reload restart                                ;;
    esac
else
    echo "$suspend"
    echo "$power_off"
    echo "$restart"
    echo "$lock"
    echo "$tty"
    echo "$refresh"
fi
