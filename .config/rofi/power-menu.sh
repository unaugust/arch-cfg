#!/bin/bash

read -r -d '' theme <<- 'EOF'
inputbar {
    enabled: false;
}

entry {
    enabled: false;
}
EOF

modi="/home/august/.config/rofi/power_menu_modi.sh"

rofi -show power_menu -modi "power_menu:${modi}" -theme-str "$theme" -kb-cancel Super_L
