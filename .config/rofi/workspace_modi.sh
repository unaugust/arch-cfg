#!/bin/bash

if [ "$@" ]; then
    # $1 might be a word splitting bug
    i3-msg workspace "$1" > /dev/null
else
    i3-msg -t get_workspaces | jq '.[].name' | sed 's/\"//g'
fi
