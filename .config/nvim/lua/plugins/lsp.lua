local linters = require 'plugins.lint'
return {
    -- bash, sh         | bashls {{{1
    bashls = {},
    -- c, cpp           | clangd {{{1
    clangd = {},
    -- cmake            | cmake {{{1
    cmake = {},
    -- lua              | sumneko_lua {{{1
    sumneko_lua = {
        -- FIXME rename is broken. something about settings.workspace.library?
        root_dir = function(fname)
            local util = require 'lspconfig.util'
            local pattern_ancestor = util.root_pattern('.git', '.cfg', '../nvim/init.lua')
            return pattern_ancestor(fname) or util.path.dirname(fname)
        end,
        settings = {
            Lua = {
                diagnostics = {
                    globals = {'vim', 'use'},
                    disable = {'lowercase-global'},
                },
                runtime = {
                    version = 'LuaJIT',
                    path = {
                        'lua/?.lua',
                        'lua/?/init.lua',
                        unpack(vim.split(package.path, ';'))
                    },
                },
                workspace = {
                    checkThirdParty = false,
                    library = vim.api.nvim_get_runtime_file('', true)
                },
                telemetry = {
                    enable = false,
                },
            },
        }
    },
    -- nix              | rnix-lsp {{{1
    rnix = {},
    -- python           | pyright {{{1
    pyright = {},
    -- rust             | rust_analyzer {{{1
    rust_analyzer = {},
    -- lint / format    | efm {{{1
    efm = {
        filetypes = vim.tbl_keys(linters),
        init_options = {
            documentFormatting = false,
        },
        settings = {
            linters = linters,
            log_level = 4,
            log_file = '/tmp/efm.log',
        },
    },
    -- }}}1
}

-- vim: foldmethod=marker
