local tools = {
    -- python           | flake8 {{{1
    flake8 = {
        lintCommand = 'flake8 --stdin-display-name ${INPUT} -',
        lintFormats = {'%f:%l:%c: %m'},
        lintIgnoreExitCode = true,
        lintStdin = true,
    },
    -- sh, bash         | shellcheck {{{1
    shellcheck = {
        lintCommand = 'shellcheck -f gcc -x ${INPUT}',
        lintSource = 'shellcheck',
        lintFormats = {
            '%f:%l:%c: %trror: %m',
            '%f:%l:%c: %tarning: %m',
            '%f:%l:%c: %tote: %m',
        },
    },
    -- }}}1
}

return {
    sh = {tools.shellcheck},
    bash = {tools.shellcheck},
    python = {tools.flake8},
}
