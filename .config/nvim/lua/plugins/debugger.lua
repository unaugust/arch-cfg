return {
    -- lldb         | c,c++,rust {{{1
    lldb = {
        adapter = {
            type = 'executable',
            command = '/usr/bin/lldb-vscode',
            name = 'lldb',
        },
        cfg = {
            name = 'Launch',
            type = 'lldb',
            request = 'launch',
            program = function()
                return vim.fn.input(
                    'Path to executable: ', vim.fn.getcwd() .. '/', 'file')
            end,
            cwd = '${workspaceFolder}',
            stopOnEntry = false,
            args = {},
            runInTerminal = false,
        },
    }
    -- }}}1
}

-- vim: foldmethod=marker
