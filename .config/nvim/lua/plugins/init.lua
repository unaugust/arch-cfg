local M = {}

M.autopairs = function()
    local autopairs = require 'nvim-autopairs'
    autopairs.setup {
        check_ts = true,
        ts_config = {},
    }
end
M.bufferline = function()
    local bufferline = require 'bufferline'
    bufferline.setup {
        options = {
            diagnostics = 'nvim_lsp',
            separator_style = 'slant',
            show_buffer_icons = true,
            show_buffer_close_icons = false,
            show_close_icon = false,
        }
    }
    util.keybind {
        n = {
            ['[b'] = util.bind(bufferline.cycle, -1),
            [']b'] = util.bind(bufferline.cycle, 1),
            ['[B'] = util.bind(bufferline.move, -1),
            [']B'] = util.bind(bufferline.move, 1),
        },
        t = {
            ['[b'] = util.bind(bufferline.cycle, -1),
            [']b'] = util.bind(bufferline.cycle, 1),
            ['[B'] = util.bind(bufferline.move, -1),
            [']B'] = util.bind(bufferline.move, 1),
        },
    }
end
M.colorizer = function()
    local colorizer = require 'colorizer'
    colorizer.setup(
        { '*', css = { css = true, }, },
        { RRGGBBAA = true, names = false, }
    )
end
M.comment = function()
    local comment = require 'Comment'
    comment.setup {}
end
M.cmp = function()
    local cmp = require 'cmp'
    local luasnip = require 'luasnip'
    local lspkind = require 'lspkind'
    local cmp_autopairs = require 'nvim-autopairs.completion.cmp'

    local map = cmp.mapping
    local pum = function(vis)
        return function(fallback)
            if cmp.visible() then vis() else fallback() end
        end
    end

    local keymaps = {
        ['<C-q>'] = map.confirm {
            behavior = cmp.ConfirmBehavior.Replace, select = true, },
        ["<C-n>"] = map.select_next_item {
            behavior = cmp.SelectBehavior.Insert, },
        ["<C-p>"] = map.select_prev_item {
            behavior = cmp.SelectBehavior.Insert, },
        ['<Tab>'] = pum(cmp.select_next_item),
        ['<S-Tab>'] = pum(cmp.select_prev_item),
        ["<C-d>"] = map.scroll_docs(-4),
        ["<C-f>"] = map.scroll_docs(4),
        ["<C-e>"] = map.close(),
        ["<C-y>"] = map(
            map.confirm { behavior = cmp.ConfirmBehavior.Insert, select = true, },
            { "i", "c" }),
        ['<CR>'] = pum(cmp.confirm),
        ['<C-Space>'] = map.complete(),
    }

    cmp.setup {
        experimental = {
            native_menu = false,
            ghost_text = true,
        },
        formatting = {
            format = lspkind.cmp_format {
                mode = 'symbol_text',
                maxwidth = 50,
                --[[ for debug
                menu = {
                    buffer = "[buf]",
                    nvim_lsp = "[lsp]",
                    nvim_lua = "[api]",
                    path = "[path]",
                    luasnip = "[snip]",
                },
                --]]
            }
        },
        mapping = keymaps,
        snippet = {
            expand = function(args) luasnip.lsp_expand(args.body) end,
        },
        sources = {
            { name = 'nvim_lsp_signature_help'    },
            { name = 'nvim_lua'                   },
            { name = 'nvim_lsp'                   },
            { name = 'path'                       },
            { name = 'luasnip'                    },
            { name = 'buffer', keyword_length = 5 },
        },
        view = {
            entries = { name = 'custom', selection_order = 'near_cursor' }
        },
        window = {
            completion = cmp.config.window.bordered {
                winhighlight = 'Normal:Normal'
            },
            documentation = cmp.config.window.bordered {
                winhighlight = 'Normal:Normal'
            },
        },
    }

    cmp.setup.cmdline(':', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = cmp.config.sources(
            { { name = 'path' }, },
            { { name = 'cmdline' }, })
    })

    cmp.setup.cmdline('/', {
        mapping = cmp.mapping.preset.cmdline(),
        sources = { { name = 'buffer' } }
    })

    cmp.event:on(
        'confirm_done', cmp_autopairs.on_confirm_done { map_char = { tex = '' }})
    util.highlight(require 'theme.cmp')
end
M.crates = function()
    local crates = require'crates'
    local cmp require 'cmp'
    crates.setup {}
    cmp.setup.buffer {
        sources = {
            { name = 'crates' },
            { name = 'buffer' },
        },
    }
end
M.dap = function()
    local cfg = require 'plugins.debugger'
    local dap = require 'dap'
    local fun = util.fun
    vim.fn.sign_define('DapBreakpoint', {
        text = '',
        texthl = 'Debug',
        linehl = '',
        numhl = 'Debug',
    })
    dap.adapters = fun.iter(cfg)
        :map(function(k, v) return k, v.adapter end)
        :tomap()
    dap.configurations = {
        c = {cfg.lldb.cfg},
        cpp = {cfg.lldb.cfg},
        rust = {cfg.lldb.cfg},
    }
    vim.g.mapleader = [[\]]
    util.keybind {
        n = {
            ['<F5>'] = dap.continue,
            ['<F10>'] = dap.step_out,
            ['<F11>'] = dap.step_into,
            ['<F12>'] = dap.step_out,
            ['<leader>b'] = dap.toggle_breakpoint,
            ['<leader>B'] = function()
                dap.toggle_breakpoint(vim.fn.input('Breakpont condition: '))
            end,
        }
    }
end
M.easy_align = function()
    util.keybind {
        n = { ['ga'] = '<Plug>(EasyAlign)' },
        x = { ['ga'] = '<Plug>(EasyAlign)' },
    }
end
M.gitsigns = function()
    local gitsigns = require 'gitsigns'
    gitsigns.setup {
        signs = {
            add = {
                hl = 'GitSignsAdd',
                text = '│',     -- u+2502
                numhl='GitSignsAddNr',
                linehl='GitSignsAddLn',
            },
            change = {
                hl = 'GitSignsChange',
                text = '│',
                numhl='GitSignsChangeNr',
                linehl='GitSignsChangeLn',
            },
            delete = {
                hl = 'GitSignsDelete',
                text = '│',
                numhl='GitSignsDeleteNr',
                linehl='GitSignsDeleteLn',
            },
            topdelete = {
                hl = 'GitSignsDelete',
                text = '‾',
                numhl='GitSignsDeleteNr',
                linehl='GitSignsDeleteLn',
            },
            changedelete = {
                hl = 'GitSignsChange',
                text = '│',
                numhl='GitSignsChangeNr',
                linehl='GitSignsChangeLn',
            },
        }
    }
end
M.indent_blankline = function()
    local indent = require 'indent_blankline'
    indent.setup {
        char = '',
        context_char = '┊',
        show_current_context = true,
        show_first_indent_level = false,
        use_treesitter = true,
    }
end
M.lspconfig = function()
    vim.lsp.handlers['textDocument/publishDiagnostics'] = vim.lsp.with(
        vim.lsp.diagnostic.on_publish_diagnostics,
        {
            virtual_text = {
                spacing = 4,
                prefix = '',
            }
        }
    )
    for _, e in ipairs{'Error', 'Warn', 'Information', 'Hint'} do
        local name = 'DiagnosticSign' .. e
        vim.fn.sign_define(name, { text = '', texthl = name, numhl = name })
    end
    local lspconfig = require 'lspconfig'
    local lsp_cfg = require 'plugins.lsp'
    local cmp_lsp = require 'cmp_nvim_lsp'
    local capabilities =
        cmp_lsp.update_capabilities(vim.lsp.protocol.make_client_capabilities())
    for lsp, cfg in pairs(lsp_cfg) do
        cfg.capabilities = capabilities
        lspconfig[lsp].setup(cfg)
    end
end
M.lualine = function()
    local lualine = require 'lualine'
    lualine.setup {
        options = {
            component_separators = { left = '', right = ''},
            globalstatus = true,
            lower = true,
            section_separators = { left = '', right = ''},
            theme = require 'theme.lualine',
        },
        sections = {
            lualine_a = {'mode'},
            lualine_b = {
                    { 'b:gitsigns_head', icon = '' }
            },
            lualine_c = {
                {
                    'diff',
                    symbols = {
                        added = '',
                        modified = 'ﰣ',
                        removed = '',
                    }
                },
                {
                    'filename',
                    symbols = {
                        modified = ' ●',
                        readonly = ' ',
                        unamed = '',
                    }
                },
                {
                    'lsp_progress'
                },
            },
            lualine_x = {
                {
                    'diagnostics',
                    sources = {'nvim_diagnostic'},
                    symbols = {
                        error = ' ',
                        warn = ' ',
                        info = ' ',
                        hint = ' ',
                    },
                },
            },
            lualine_y = {'filetype'},
            lualine_z = {'location'},
        },
    }
end
M.luasnip = function()
    local luasnip = require 'luasnip'
    luasnip.config.setup {}
end
M.neorg = function()
    local neorg = require 'neorg'
    neorg.setup {
        load = {
            ['core.defaults'] = {},
            ['core.integrations.nvim-cmp'] = {},
            ['core.norg.concealer'] = {},
        }
    }
end
M.packer = function()
    local packer = require 'packer'
    local reload = require 'plenary.reload'
    util.autocmd.augroup('user', {clear = false}) {
        BufWritePost = {
            ['plugins/*.lua'] = function()
                reload.reload_module 'plugins'
                require 'plugins'
                packer.compile()
            end,
        },
    }
end
M.packer_startup = {
    display = {
        header_sym = '',
        open_fn = function()
            return require 'packer.util'.float {
                border = 'rounded',
            }
        end,
    },
}
M.presence = function()
    local presence = require 'presence'
    presence:setup {}
end
M.telescope = function()
    local telescope = util.autoload('telescope')
    telescope.setup {
        defaults = {
            layout_strategy = 'flex',
            prompt_prefix = '',
            selection_caret = '',
            vimgrep_arguments = {
                'rg',
                '--color=never', '--no-heading', '--with-filename',
                '--line-number', '--column', '--smart-case', '--trim',
            },
        },
        extensions = {
            fzf = {
                fuzzy = true,
                override_generic_sorter = true,
                override_file_sorter = true,
            },
            ['ui-select'] = {
                telescope.themes.get_dropdown {},
            },
        },
        pickers = {
            find_files = {
                follow = true,
                find_command = {
                    'fd',
                    '--follow', '--type', 'f',
                    '--strip-cwd-prefix'
                },
            },
        },
    }
    telescope.load_extension 'fzf'
    telescope.load_extension 'ui-select'
    vim.g.mapleader = [[\]]
    util.highlight(require 'theme.telescope')

    local find_dotfiles = function(opts)
        local pid = io.popen('dotbare rev-parse --show-toplevel')
        local path = pid:read() or ''
        pid:close()
        telescope.pickers.new(opts or {}, {
            prompt_title = 'dotfiles',
            finder = telescope.finders.new_oneshot_job(
                {
                    'dotbare', 'ls-tree', '-r', 'HEAD',
                    '--name-only', '--full-tree'
                },
                {
                    entry_maker = function(entry)
                        return {
                            value = path .. '/' .. entry,
                            display = entry,
                            ordinal = entry,
                        }
                    end,
                }),
            sorter = telescope.config.values.file_sorter(opts or {}),
        }):find()
    end
    local in_dotfles = function()
        local pid = io.popen('dotbare ls-files')
        local ret = pid:read() ~= nil
        pid:close()
        return ret
    end
    util.keybind {
        n = {
            ['<leader>e'] = function()
                if pcall(telescope.builtin.git_files) then
                    return
                elseif in_dotfles() then
                    find_dotfiles()
                else
                    telescope.builtin.find_files()
                end
            end,
            ['<leader>fb'] = telescope.builtin.buffers,
            ['<leader>fc'] = find_dotfiles,
            ['<leader>fg'] = telescope.builtin.live_grep,
            ['<leader>fk'] = telescope.builtin.keymaps,
            ['<leader>fr'] = telescope.builtin.lsp_references,
            ['<leader>h'] = telescope.builtin.help_tags,
            ['<leader>m'] = telescope.builtin.man_pages,
        },
    }
end
M.treesitter = function()
    local ts_conf = require 'nvim-treesitter.configs'
    ts_conf.setup {
        ensure_installed = {
            'bash', 'c', 'cmake', 'cpp', 'css', 'javascript', 'json', 'lua', 'markdown',
            'markdown_inline', 'nix', 'norg', 'python', 'rasi', 'rust', 'toml',
            'typescript', 'yaml',
        },
        highlight = { enable = true },
    }
    --[[
    -- fold lua comments
    -- uncomment when neovim/neovim#14932 gets merged
    local query = require 'vim.treesitter.query'
    query.set_query_files('lua', 'folds', {'~/.config/nvim/lua/plugins/lua-folds.scm'})
    ]]
end
M.virt_column = function()
    local vcolumn = require 'virt-column'
    vcolumn.setup {
        char = '│',
    }
    util.highlight {
        VirtColumn = {
            fg = require 'theme.colors'.base01
        }
    }
end

return M
