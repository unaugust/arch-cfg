local fun = util.fun
local colors = require 'theme.colors'

local mode_colors = {
    normal = colors.blue,
    insert = colors.green,
    visual = colors.orange,
    replace = colors.red,
    command = colors.yellow,
}

return fun.iter(mode_colors)
    :map(function(k,v)
        return k, {
            a = { bg = v,               fg = colors.bg, gui = 'bold' },
            b = { bg = colors.light_bg, fg = colors.fg },
            c = { bg = colors.bg,       fg = colors.fg },
        }
    end)
    :tomap()
