local colors = require 'theme.colors'

local color_map = {
    Selection        = { fg = colors.yellow, bg = 'None', bold = true, },
    SelectionCaret  = { fg = colors.red,    bg = 'None' },
    MultiSelection  = { fg = colors.yellow, bg = 'None' },
    Border          = { fg = colors.base03, bg = 'None' },
    PromptBorder    = { fg = colors.base03, bg = 'None' },
    ResultsBorder   = { fg = colors.base03, bg = 'None' },
    PreviewBorder   = { fg = colors.base03, bg = 'None' },
    Matching        = { fg = colors.blue,   bg = 'None' },
    PromptPrefix    = { fg = colors.red,    bg = 'None' },
}

return util.fun.iter(color_map)
    :map(function(k, v) return 'Telescope' .. k, v end)
    :tomap()
