local fun = util.fun
local bind = util.bind
local core = fun.zip(
        fun.range(0,15)
            :map(bind(string.format, 'base%02X')),
        fun.iter(require 'theme.base16')
            :map(bind(fun.op.concat, '#')))
    :tomap()

local aliases = fun.iter {
    background          = 0x0,
    bg                  = 0x0,
    light_background    = 0x1,
    light_bg            = 0x1,
    selection           = 0x2,
    line_highlight      = 0x3,
    dark_foreground     = 0x4,
    dark_fg             = 0x4,
    foreground          = 0x5,
    fg                  = 0x5,

    black               = 0x0,
    grey                = 0x3,
    gray                = 0x3,
    white               = 0x5,
    red                 = 0x8,
    orange              = 0x9,
    yellow              = 0xa,
    green               = 0xb,
    cyan                = 0xc,
    blue                = 0xd,
    magenta             = 0xe,
    purple              = 0xe,
    brown               = 0xf,
}

return aliases
    :map(function(k,v) return k, core[string.format('base0%X', v)] end)
    :chain(core)
    :tomap()
