local colors = require 'theme.colors'
util.highlight {
    -- Standard UI {{{1
    Normal = { -- {{{2
        fg = colors.base05,
        bg = colors.base00,
        sp = "None",
    },
    Bold = { -- {{{2
        fg = "None",
        bg = "None",
        bold = true,
        sp = "None",
    },
    Debug = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },
    Directory = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },
    Error = { -- {{{2
        fg = colors.base00,
        bg = colors.base08,
        sp = "None",
    },
    ErrorMsg = { -- {{{2
        fg = colors.base08,
        bg = colors.base00,
        sp = "None",
    },
    Exception = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },
    FoldColumn = { -- {{{2
        fg = colors.base0C,
        bg = colors.base01,
        sp = "None",
    },
    Folded = { -- {{{2
        bg = 'None',
        fg = colors.base02,
        sp = "None",
    },
    IncSearch = { -- {{{2
        fg = colors.base01,
        bg = colors.base09,
        none = true,
        sp = "None",
    },
    Italic = { -- {{{2
        fg = "None",
        bg = "None",
        none = true,
        sp = "None",
    },
    Macro = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },
    MatchParen = { -- {{{2
        fg = "None",
        bg = colors.base03,
        sp = "None",
    },
    ModeMsg = { -- {{{2
        fg = colors.base0B,
        bg = "None",
        sp = "None",
    },
    MoreMsg = { -- {{{2
        fg = colors.base0B,
        bg = "None",
        sp = "None",
    },
    Question = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },
    Search = { -- {{{2
        fg = colors.base01,
        bg = colors.base0A,
        sp = "None",
    },
    Substitute = { -- {{{2
        fg = colors.base01,
        bg = colors.base0A,
        none = true,
        sp = "None",
    },
    SpecialKey = { -- {{{2
        fg = colors.base03,
        bg = "None",
        sp = "None",
    },
    TooLong = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },
    Underlined = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },
    Visual = { -- {{{2
        fg = "None",
        bg = colors.base02,
        sp = "None",
    },
    VisualNOS = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },
    WarningMsg = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },
    WildMenu = { -- {{{2
        fg = colors.base08,
        bg = colors.base0A,
        sp = "None",
    },
    Title = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        none = true,
        sp = "None",
    },
    Conceal = { -- {{{2
        fg = colors.base0D,
        bg = colors.base00,
        sp = "None",
    },
    Cursor = { -- {{{2
        fg = colors.base00,
        bg = colors.base05,
        sp = "None",
    },
    NonText = { -- {{{2
        fg = colors.base03,
        bg = "None",
        sp = "None",
    },
    LineNr = { -- {{{2
        fg = colors.base02,
        bg = 'None',
        sp = "None",
    },
    SignColumn = { -- {{{2
        fg = colors.base02,
        bg = 'None',
        sp = "None",
    },
    StatusLine = { -- {{{2
        fg = colors.base04,
        bg = colors.base02,
        none = true,
        sp = "None",
    },
    StatusLineNC = { -- {{{2
        fg = colors.base03,
        bg = colors.base01,
        none = true,
        sp = "None",
    },
    ColorColumn = { -- {{{2
        fg = colors.base01,
        bg = "None",
        none = true,
        sp = "None",
    },
    CursorColumn = { -- {{{2
        fg = "None",
        bg = colors.base01,
        none = true,
        sp = "None",
    },
    CursorLine = { -- {{{2
        fg = "None",
        bg = colors.base01,
        none = true,
        sp = "None",
    },
    CursorLineNr = { -- {{{2
        fg = colors.base04,
        bg = 'None',
        sp = "None",
    },
    QuickFixLine = { -- {{{2
        fg = "None",
        bg = colors.base01,
        none = true,
        sp = "None",
    },
    PMenu = { -- {{{2
        fg = colors.base05,
        bg = colors.base01,
        none = true,
        sp = "None",
    },
    PMenuSel = { -- {{{2
        fg = colors.base01,
        bg = colors.base05,
        sp = "None",
    },
    TabLine = { -- {{{2
        fg = colors.base03,
        bg = colors.base01,
        none = true,
        sp = "None",
    },
    TabLineFill = { -- {{{2
        fg = colors.base03,
        bg = colors.base01,
        none = true,
        sp = "None",
    },
    TabLineSel = { -- {{{2
        fg = colors.base0B,
        bg = colors.base01,
        none = true,
        sp = "None",
    },
    WinSeparator = { -- {{{2
        bg = 'None',
        fg = colors.base01,
    },

    -- Spelling {{{1
    SpellBad = { -- {{{2
        fg = "None",
        bg = "None",
        undercurl = true,
        sp = colors.base08,
    },
    SpellLocal = { -- {{{2
        fg = "None",
        bg = "None",
        undercurl = true,
        sp = colors.base0C,
    },
    SpellCap = { -- {{{2
        fg = "None",
        bg = "None",
        undercurl = true,
        sp = colors.base0D,
    },
    SpellRare = { -- {{{2
        fg = "None",
        bg = "None",
        undercurl = true,
        sp = colors.base0E,
    },

    -- Standard syntax {{{1
    Boolean = { -- {{{2
        fg = colors.base09,
        bg = "None",
        sp = "None",
    },
    Character = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },
    Comment = { -- {{{2
        fg = colors.base03,
        bg = "None",
        sp = "None",
    },
    Conditional = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    Constant = { -- {{{2
        fg = colors.base09,
        bg = "None",
        sp = "None",
    },
    Define = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        none = true,
        sp = "None",
    },
    Delimiter = { -- {{{2
        fg = colors.base0F,
        bg = "None",
        sp = "None",
    },
    Float = { -- {{{2
        fg = colors.base09,
        bg = "None",
        sp = "None",
    },
    Function = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },
    Identifier = { -- {{{2
        fg = colors.base08,
        bg = "None",
        none = true,
        sp = "None",
    },
    Include = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },
    Keyword = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    Label = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },
    Number = { -- {{{2
        fg = colors.base09,
        bg = "None",
        sp = "None",
    },
    Operator = { -- {{{2
        fg = colors.base05,
        bg = "None",
        none = true,
        sp = "None",
    },
    PreProc = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },
    Repeat = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },
    Special = { -- {{{2
        fg = colors.base0C,
        bg = "None",
        sp = "None",
    },
    SpecialChar = { -- {{{2
        fg = colors.base0F,
        bg = "None",
        sp = "None",
    },
    Statement = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },
    StorageClass = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },
    String = { -- {{{2
        fg = colors.base0B,
        bg = "None",
        sp = "None",
    },
    Structure = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    Tag = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },
    Todo = { -- {{{2
        fg = colors.base0A,
        bg = colors.base01,
        sp = "None",
    },
    Type = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        none = true,
        sp = "None",
    },
    Typedef = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },

    -- C {{{1
    cOperator = { -- {{{2
        fg = colors.base0C,
        bg = "None",
        sp = "None",
    },
    cPreCondit = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },

    -- C# {{{1
    csClass = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },
    csAttribute = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },
    csModifier = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    csType = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },
    csUnspecifiedStatement = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },
    csContextualStatement = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    csNewDecleration = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },

    -- CSS {{{1
    cssBraces = { -- {{{2
        fg = colors.base05,
        bg = "None",
        sp = "None",
    },
    cssClassName = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    cssColor = { -- {{{2
        fg = colors.base0C,
        bg = "None",
        sp = "None",
    },

    -- Diff {{{1
    DiffAdd = { -- {{{2
        link = 'DiffAdded'
    },
    DiffChange = { -- {{{2
        fg = colors.base09,
        bg = colors.base00,
        sp = "None",
    },
    DiffDelete = { -- {{{2
        link = 'DiffRemoved'
    },
    DiffText = { -- {{{2
        fg = colors.base0D,
        bg = colors.base01,
        sp = "None",
    },
    DiffAdded = { -- {{{2
        fg = colors.base0B,
        bg = colors.base00,
        sp = "None",
    },
    DiffFile = { -- {{{2
        fg = colors.base08,
        bg = colors.base00,
        sp = "None",
    },
    DiffNewFile = { -- {{{2
        fg = colors.base0B,
        bg = colors.base00,
        sp = "None",
    },
    DiffLine = { -- {{{2
        fg = colors.base0D,
        bg = colors.base00,
        sp = "None",
    },
    DiffRemoved = { -- {{{2
        fg = colors.base08,
        bg = colors.base00,
        sp = "None",
    },

    -- Git {{{1
    gitcommitOverflow = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },
    gitcommitSummary = { -- {{{2
        fg = colors.base0B,
        bg = "None",
        sp = "None",
    },
    gitcommitComment = { -- {{{2
        fg = colors.base03,
        bg = "None",
        sp = "None",
    },
    gitcommitUntracked = { -- {{{2
        fg = colors.base03,
        bg = "None",
        sp = "None",
    },
    gitcommitDiscarded = { -- {{{2
        fg = colors.base03,
        bg = "None",
        sp = "None",
    },
    gitcommitSelected = { -- {{{2
        fg = colors.base03,
        bg = "None",
        sp = "None",
    },
    gitcommitHeader = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    gitcommitSelectedType = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },
    gitcommitUnmergedType = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },
    gitcommitDiscardedType = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },
    gitcommitBranch = { -- {{{2
        fg = colors.base09,
        bg = "None",
        bold = true,
        sp = "None",
    },
    gitcommitUntrackedFile = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },
    gitcommitUnmergedFile = { -- {{{2
        fg = colors.base08,
        bg = "None",
        bold = true,
        sp = "None",
    },
    gitcommitDiscardedFile = { -- {{{2
        fg = colors.base08,
        bg = "None",
        bold = true,
        sp = "None",
    },
    gitcommitSelectedFile = { -- {{{2
        fg = colors.base0B,
        bg = "None",
        bold = true,
        sp = "None",
    },

    -- HTML {{{1
    htmlBold = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },
    htmlItalic = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    htmlEndTag = { -- {{{2
        fg = colors.base05,
        bg = "None",
        sp = "None",
    },
    htmlTag = { -- {{{2
        fg = colors.base05,
        bg = "None",
        sp = "None",
    },

    -- Java {{{1
    javaOperator = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },
    -- JavaScript {{{1
    javaScript = { -- {{{2
        fg = colors.base05,
        bg = "None",
        sp = "None",
    },
    javaScriptBraces = { -- {{{2
        fg = colors.base05,
        bg = "None",
        sp = "None",
    },
    javaScriptNumber = { -- {{{2
        fg = colors.base09,
        bg = "None",
        sp = "None",
    },

    -- Mail {{{1
    mailQuoted1 = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },
    mailQuoted2 = { -- {{{2
        fg = colors.base0B,
        bg = "None",
        sp = "None",
    },
    mailQuoted3 = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    mailQuoted4 = { -- {{{2
        fg = colors.base0C,
        bg = "None",
        sp = "None",
    },
    mailQuoted5 = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },
    mailQuoted6 = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },
    mailURL = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },
    mailEmail = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },

    -- Markdown {{{1
    markdownCode = { -- {{{2
        fg = colors.base0B,
        bg = "None",
        sp = "None",
    },
    markdownError = { -- {{{2
        fg = colors.base05,
        bg = colors.base00,
        sp = "None",
    },
    markdownCodeBlock = { -- {{{2
        fg = colors.base0B,
        bg = "None",
        sp = "None",
    },
    markdownHeadingDelimiter = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },

    -- PHP {{{1
    phpMemberSelector = { -- {{{2
        fg = colors.base05,
        bg = "None",
        sp = "None",
    },
    phpComparison = { -- {{{2
        fg = colors.base05,
        bg = "None",
        sp = "None",
    },
    phpParent = { -- {{{2
        fg = colors.base05,
        bg = "None",
        sp = "None",
    },
    phpMethodsVar = { -- {{{2
        fg = colors.base0C,
        bg = "None",
        sp = "None",
    },

    -- Python {{{1
    pythonOperator = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    pythonRepeat = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    pythonInclude = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    pythonStatement = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },

    -- Ruby {{{1
    rubyAttribute = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },
    rubyConstant = { -- {{{2
        fg = colors.base0A,
        bg = "None",
        sp = "None",
    },
    rubyInterpolationDelimiter = { -- {{{2
        fg = colors.base0F,
        bg = "None",
        sp = "None",
    },
    rubyRegexp = { -- {{{2
        fg = colors.base0C,
        bg = "None",
        sp = "None",
    },
    rubySymbol = { -- {{{2
        fg = colors.base0B,
        bg = "None",
        sp = "None",
    },
    rubyStringDelimiter = { -- {{{2
        fg = colors.base0B,
        bg = "None",
        sp = "None",
    },

    -- SASS {{{1
    sassidChar = { -- {{{2
        fg = colors.base08,
        bg = "None",
        sp = "None",
    },
    sassClassChar = { -- {{{2
        fg = colors.base09,
        bg = "None",
        sp = "None",
    },
    sassInclude = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    sassMixing = { -- {{{2
        fg = colors.base0E,
        bg = "None",
        sp = "None",
    },
    sassMixinName = { -- {{{2
        fg = colors.base0D,
        bg = "None",
        sp = "None",
    },

    -- }}}
    -- }}}1
}

-- vim: foldmethod=marker
