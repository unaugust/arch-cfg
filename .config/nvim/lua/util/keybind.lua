local common = require'util._common'
local M = {}

local as_bind_object = function(expr)
    return common.choose(
        type(expr) == 'table' and expr.options,
        expr,
        {expr, options = {}})
end

local bind_opts = {'buffer', 'expr', 'nowait', 'remap', 'script', 'silent', 'unique'}
for _, opt in ipairs(bind_opts) do
    M[opt] = function(expr)
        local b = as_bind_object(expr)
        b.options[opt] = true
        return b
    end
end

M.pumvisible = function(pum, nopum)
    return common.choose(vim.fn.pumvisible() == 0, nopum, pum)
end

M.pum_expr = function(pum, nopum)
    return M.expr(common.bind(M.pumvisible, pum, nopum))
end

return setmetatable(M, { __call = function(_, keybinds)
    for mode, kbs in pairs(keybinds) do
        for k, v in pairs(kbs) do
            local b = as_bind_object(v)
            vim.keymap.set(mode, k, b[1], b.options)
        end
    end
end })
