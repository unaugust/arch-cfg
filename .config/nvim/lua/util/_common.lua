local M = {}

M.Set = function(list)
    local self = {}
    for _, v in ipairs(list) do
        self[v] = true
    end
    return self
end

M.choose = function(pred, if_true, if_false)
    if pred then
        return if_true
    else
        return if_false
    end
end

M.bind = function(fn, ...)
    local bound_args = {...}
    return function(...)
        local args = vim.list_extend(vim.deepcopy(bound_args), {...})
        return fn(unpack(args))
    end
end

M.pipe = function(f1, f2, ...)
    local fns = {f1, f2, ...}
    local pipe = function(piped, fn)
        return function(...) return fn(piped(...)) end
    end
    local pipeline = M.fold(pipe, fns, function(...) return ... end)
    return function(...)
        return pipeline(...)
    end
end

M.is_root = function() return vim.env.USER == 'root' end

M.foldtext = function()
    local fold = {
        start = vim.v.foldstart,
        finish = vim.v.foldend,
        marker = vim.o.foldmarker:match([[^(.*),]]) .. [[%d?]],
        fillchar = vim.o.fillchars:match([[fold:(.)]]) or ' ',
        size = function(self) return self.finish - self.start end,
    }
    local line_text = (function()
        local first_line_text = vim.fn.getline(fold.start)
            :gsub([[^(.*)%s*]] .. fold.marker .. [[%s*$]], [[%1]])
            :gsub([[^]] .. string.rep(' ', vim.o.tabstop), '')
        local last_line_text = vim.fn.getline(fold.finish)
            :gsub([[^%s*]], '')
        if last_line_text:len() < 12 and vim.o.foldmethod ~= 'marker' then
            return first_line_text .. ' … ' .. last_line_text
        else
            return first_line_text
        end
    end)()
    local filler_width = vim.o.textwidth
        - vim.fn.strchars(line_text)    -- multi-byte chars mess up strlen
        - tostring(fold:size()):len()
    return string.format(' 祈 %s%s%sℓ ',
        line_text,
        fold.fillchar:rep(filler_width - 6),
        fold:size())
end

M.abbreviate = function(ab_table)
--- define abbreviation
-- takes table mapping modes (c,i), to tables mapping abbreviation triggers to either
-- abbreviation expansions or rhs tables. an rhs table must have a val field containing
-- the abbreviation expansion, and may also have the boolean fields for buffer, expr, and
-- remap.
-- @usage abbreviate {
--      c = {
--          spt = SplitTerm,
--          vst = VsplitTerm,
--      },
--      i = {
--          file = {
--              val = [[expand('%')]],
--              expr = true,
--              buffer = true,
--          }
--      }
--  }
-- @tparam { [string]={ [string]=string|{ [string]=string|bool } } }
-- @see :h :ab
    for mode, abbrs in pairs(ab_table) do
        for lhs, rhs in pairs(abbrs) do
            rhs = M.choose(type(rhs) == 'string', { val = rhs }, rhs)
            vim.cmd(string.format('%s%sabbrev %s %s %s %s',
                mode,
                M.choose(rhs.remap, '', 'nore'),
                M.choose(rhs.buffer, '<buffer>', ''),
                M.choose(rhs.expr, '<expr>', ''),
                lhs,
                rhs.val
            ))
        end
    end
end

return M
