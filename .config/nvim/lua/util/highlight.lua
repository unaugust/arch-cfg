local fun = require 'util.fun'
local bind = require 'util'.bind

return setmetatable({}, { __call = function(_, hl_table)
    --[[
    highlight {
        CursorColumn = {
            foreground = #8f3eaa,
        },
    }
    --]]
    -- try something like this once nvim works on it more
--    local global_hl = vim.api.nvim__get_hl_defs(0)
--    local namespace = vim.api.nvim_create_namespace('Dotfile')
--    fun.iter(hl_table):each(function(group, args)
--        vim.api.nvim_set_hl(namespace, group, args)
--    end)

    fun.iter(hl_table):each(function(group, args)
        if args.link then
            vim.cmd(string.format('highlight%s link %s %s',
                args.force and '!' or '',
                group,
                args.link))
        else
            local cterm_attrs = { 'bold', 'underline', 'underlineline', 'undercurl',
				'underdot', 'underdash', 'strikethrough', 'reverse', 'inverse', 'italic',
				'standout', 'nocombine',
            }
            local format_args = (function()
                local iter = fun.iter(args)
                    :filter(function(k, v)
                        return v == true and vim.tbl_contains(cterm_attrs, k)
                    end)
                    :map(function(k, _) return k end)
                    :intersperse(',')
                if iter:is_null() then
                    return nil
                else
                    iter:foldl(fun.op.concat, '')
                end
            end)()
            local hi_args = {
                guifg = args.foreground or args.fg or args.guifg,
                guibg = args.background or args.bg or args.guibg,
                guisp = args.special    or args.sp or args.guisp,
                gui = format_args,
                blend = args.blend,
            }
            vim.cmd(fun.iter(hi_args)
                :map(bind(string.format, '%s=%s'))
                :intersperse(' ')
                :foldl(fun.op.concat, 'highlight ' .. group .. ' '))
        end
    end)
end })

