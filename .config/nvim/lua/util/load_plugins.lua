local bootstrap = function()
    local fn = vim.fn
    local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'

    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({'git', 'clone',
            'https://github.com/wbthomason/packer.nvim', install_path})
        vim.cmd 'packadd packer.nvim'
    end

    local packer = require 'packer'
    if packer then
        packer.install()
        packer.sync()
    end
    return packer
end

local startup = function(pack_specs, packer)
    local cfg = require 'plugins'
    packer.startup {
        util.fun.iter(pack_specs)
            :map(function(k, v)
                return vim.tbl_extend('error', v, { config = cfg[k] })
            end)
            :totable(),
        config = cfg.packer_startup,
    }
end

local Maybe = util.maybe

return setmetatable({}, { __call = function(_, pack_specs)
    Maybe:try(require, 'packer')
        :or_else_try(bootstrap)
        :or_else_try(util.bind(vim.notify, "couldn't bootstrap packer :(", 'error'))
        :map(function(packer) startup(pack_specs, packer) end)
end})
