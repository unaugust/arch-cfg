local common = require'util._common'
local M = {}

local as_cmd_object = function(cmd)
    return common.choose(
        type(cmd) == 'table' and (cmd.command or cmd.callback),
        cmd,
        { [common.choose(type(cmd) == 'string', 'command', 'callback')] = cmd })
end

for _, opt in ipairs{'once', 'nested'} do
    M[opt] = function(cmd)
        return vim.tbl_extend('force', as_cmd_object(cmd), {[opt] = true})
    end
end

for _, opt in ipairs{'group', 'pattern', 'buffer', 'desc'} do
    M[opt] = function(val, cmd)
        return vim.tbl_extend('force', as_cmd_object(cmd), {[opt] = val})
    end
end

M.augroup = function(name, opts)
    local gid
    if name then
        gid = vim.api.nvim_create_augroup(name, opts or {})
    end
    return function(aucommands)
        for event, cmds in pairs(aucommands) do
            for pattern, cmd in pairs(cmds) do
                vim.api.nvim_create_autocmd(
                    event,
                    vim.tbl_extend('keep', as_cmd_object(cmd), {
                        pattern = pattern,
                        group = gid,
                    }))
            end
        end
    end
end

return setmetatable(M, { __call = function(_, a) M.augroup()(a) end })
