-- Automatically require submodules as needed
local fun = require 'util.fun'

return function(mod, base)
    local storage = {}
    return setmetatable(
        fun.iter(base or require(mod))
            :filter(function(k, _) return not vim.startswith(k, '_') end)
            :tomap(),
        {
            __index = function(_, key)
                if not storage[key] then
                    storage[key] = require(mod .. '.' .. key)
                end
                return storage[key]
            end
        })
end
