local common = require'util._common'
local M = {}

local filetype_blacklist = common.Set {
    'diff', 'gitcommit',
}

local filepath_blacklist = {
    '/tmp/*',
}

local is_specal_buffer = function()
    return vim.bo.buftype ~= ''
end

local is_blacklisted_filetype = function()
    return filetype_blacklist[vim.bo.filetype] ~= nil
end

local is_blacklisted_path = function()
    local filename = vim.api.nvim_buf_get_name(0)
    for _, v in ipairs(filepath_blacklist) do
        local re = vim.regex(vim.fn.glob2regpat(v))
        if re:match_str(filename) then
	        return true
        end
    end
    return false
end

local is_empty_file = function()
    return vim.api.nvim_buf_get_name(0) == ''
end

local should_mkview = function()
    return not is_specal_buffer() and
           not is_blacklisted_filetype() and
           not is_blacklisted_path() and
           not is_empty_file() and
           not common.is_root()
end

M.mkview = function()
    if should_mkview() then vim.cmd 'mkview' end
end

M.loadview = function()
    if should_mkview() then vim.cmd 'silent! loadview' end
end

return M
