local Maybe = require 'util.maybe'
local bind = require 'util._common'.bind
local bootstrap = function()
    local cfg = vim.env.XDG_CONFIG_HOME or '~/.config'
    print('downloading luafun...')
    vim.fn.system({'curl',
        'https://raw.githubusercontent.com/luafun/luafun/master/fun.lua',
        '-o', cfg .. '/nvim/lua/util/_fun.lua'
    })
    return require 'util._fun'
end
return Maybe:try(require, 'util._fun')
    :or_else_try(bootstrap)
    :or_else_try(bind(vim.notify, "couldn't bootstrap luafun :(", 'error'))
    :unwrap_or(nil)
