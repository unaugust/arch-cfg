local choose = require('util._common').choose
local Maybe = {}

function Maybe:from(val)
    obj = setmetatable({_val = val or nil }, self)
    self.__index = self
    return obj
end

function Maybe:nothing()
    return self:from(nil)
end

function Maybe:just(val)
    assert(val, 'use Maybe:none() for nil value')
    return self:from(val)
end

-- fn: Callable(...) -> T
-- returns: Maybe[T]
function Maybe:try(fn, ...)
    local ok, val = pcall(fn, ...)
    return choose(ok, self:from(val), self:nothing())
end

-- self: Maybe[T]
-- fn: SafeCallable(T, ...) -> S
-- returns Maybe[S]
function Maybe:map(fn, ...)
    local args = {...}
    return self._val ~= nil
        and Maybe:from(fn(self._val, unpack(args)))
        or self
end

-- self: Maybe[T]
-- fn: Callable(T, ...) -> S
-- returns: Maybe[S]
function Maybe:and_then_try(fn, ...)
    local args = {...}
    return self._val ~= nil
        and Maybe:try(fn, self._val, unpack(args))
        or self
end

-- self: Maybe[T]
-- fn: Callable(T, ...) -> Maybe[S]
-- returns: Maybe[S]
function Maybe:and_then(fn, ...)
    local args = {...}
    return self._val ~= nil
        and fn(self._val, unpack(args))
        or self
end

-- self: Maybe[T]
-- fn: SafeCallable(T, ...) -> Maybe[S]
-- returns: Maybe[S|T]
function Maybe:or_else(fn, ...)
    local args = {...}
    return self._val ~= nil
        and self
        or fn(unpack(args))
end

-- self: Maybe[T]
-- fn: Callable(...) -> S
-- returns: Maybe[S|T]
function Maybe:or_else_try(fn, ...)
    local args = {...}
    return self._val ~= nil
        and self
        or Maybe:try(fn, unpack(args))
end

-- self: Maybe[T]
-- fallback: S
-- returns: S|T
function Maybe:unwrap_or(fallback)
    return choose(self._val ~= nil, self._val, fallback)
end

return Maybe
