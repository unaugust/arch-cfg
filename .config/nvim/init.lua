_G.util = require 'util'

------------------------------------------------------------------------------------------
-- settings {{{1
------------------------------------------------------------------------------------------

local opt = vim.opt

opt.breakindent = true            -- indent wrapped lines
opt.cinoptions = { ':0',          -- don't indent case labels
                   'g0',          -- don't indent private:/public:/protected:
                   'N-s',         -- don't indent namespace blocks
                   'E-s',         -- don't indent extern blocks
                   'p0', }        -- don't indent after 'func(a, b) <CR>'
opt.colorcolumn = '+1'            -- draw column after margin (see textwidth)
opt.completeopt = { 'menuone',    -- menu shows even if theres only one match
                    'noinsert',   -- don't insert text until match is selected
                    'noselect', } -- don't select a match by default
opt.cursorline = true             -- highlight current line
opt.cursorlineopt = 'number'      -- actually don't just highlight the line number
opt.expandtab = true              -- tabs are spaces and not tab characters
opt.fillchars = { eob = ' ',      -- (U+00A0) supress ~ at end of buffer
                  fold = ' ', }   -- no dot filler after fold name
                                  -- use treesitter for folds
opt.foldexpr = 'nvim_treesitter#foldexpr()'
opt.foldmethod = 'expr'           -- use custom function for folds
                                  -- custom display for folds
opt.foldtext = 'v:lua.require("util").foldtext()'
opt.formatoptions:append('n')     -- smart indenting for numbered lists
opt.guicursor = ''                -- disable cursor styling, leaks into terminal :(
opt.hidden = true                 -- buffers persist even when not shown
opt.inccommand = 'nosplit'        -- preview changes when using :substitute
opt.joinspaces = false            -- no redundant spaces after join
                                  -- visualized whitespcae characters (for 'list')
opt.listchars = { eol = '', lead = '·', nbsp = '⍽', tab = ' ', trail = '·' }
opt.mouse = 'a'                   -- enable full mouse controls
opt.number = true                 -- show line numbers in margin
opt.relativenumber = true         -- show current line number and distance form it
opt.scrolloff = 6                 -- min lines above or below cursor before scrolling
opt.shiftwidth = 4                -- tabs are four spaces long
opt.shortmess:append('c')         -- silence hit-enter prompt from completion menu
opt.showbreak = ' ↳ '             -- (U+21B3) show at beginning of wrapped line
opt.showmode = false              -- clean up redundant ui from airline
opt.showtabline = 2               -- always show file tab thing at the top
opt.sidescrolloff = 5             -- scrolloff but sideways
opt.signcolumn = 'yes'            -- always show gutter area for info stuff
opt.softtabstop = 4               -- no actually, tabs are 4 spaces
opt.spellcapcheck = ''            -- ignore uncapitalized sentences
opt.suffixes:remove('.h')         -- treat headers as regular files
opt.tabstop = 4                   -- tabs are 4 spaces long. i really mean it!
opt.termguicolors = true          -- make colors not green and broken
opt.textwidth = 90                -- automatically wrap at 90 characters
opt.undofile = not util.is_root   -- undo history persists between instances (no root)
opt.viewoptions = { 'folds',      -- save fold position
                    'cursor', }   -- save cursor position
opt.virtualedit = 'block'         -- select regions without characters in ^V mode
                                  -- float specific style overrides
opt.winhighlight = 'Normal:Normal'

vim.g.mapleader = [[\]]

------------------------------------------------------------------------------------------
-- highlights {{{1
------------------------------------------------------------------------------------------

require 'theme'

------------------------------------------------------------------------------------------
-- commands {{{1
------------------------------------------------------------------------------------------

local command = vim.api.nvim_create_user_command

command('SplitTerm', 'split +terminal | startinsert', {})
command('VsplitTerm', 'vsplit +terminal | startinsert', {})

------------------------------------------------------------------------------------------
-- abbreviations {{{1
------------------------------------------------------------------------------------------

util.abbreviate {
    c = {
        spt = 'SplitTerm',
        vst = 'VsplitTerm',
    },
    i = {
        teh = 'the',
        htis = 'this',
        loacl = 'local',
        fucntion = 'function',
        cosnt = 'const',
    }
}

------------------------------------------------------------------------------------------
-- keybinds {{{1
------------------------------------------------------------------------------------------

util.keybind {
    i = {
        ['lj'] = '<esc>',
        ['<C-h>'] = '<esc><C-w>h',
        ['<C-j>'] = '<esc><C-w>j',
        ['<C-k>'] = '<esc><C-w>k',
        ['<C-l>'] = '<esc><C-w>l',
    },
    n = {
        ['<leader>r'] = vim.lsp.buf.rename,
        ['<leader>s'] = vim.lsp.buf.signature_help,
        ['<leader>a'] = vim.lsp.buf.code_action,
        ['<space><space>'] = '<C-^>',
        [']e'] = vim.diagnostic.goto_next,
        ['[e'] = vim.diagnostic.goto_prev,
        [']p'] = 'ddp',
        ['[p'] = 'ddkkp',
        ['gd'] = vim.lsp.buf.definition(),
        ['K'] = vim.lsp.buf.hover,
        ['hl'] = function() vim.opt.hlsearch = not vim.o.hlsearch end,
        ['Y'] = 'y$',
        ['<C-h>'] = '<C-w>h',
        ['<C-j>'] = '<C-w>j',
        ['<C-k>'] = '<C-w>k',
        ['<C-l>'] = '<C-w>l',
        ['<C-e>'] = '<C-w>=',
        ['<C-m>'] = '<C-w>_',
    },
    t = {
        ['lj'] = [[<C-\><C-n>]], -- enter normal mode from terminal mode
        ['<C-h>'] = [[<C-\><C-n><C-w>h]],
        ['<C-j>'] = [[<C-\><C-n><C-w>j]],
        ['<C-k>'] = [[<C-\><C-n><C-w>k]],
        ['<C-l>'] = [[<C-\><C-n><C-w>l]],
    },
    v = {
        ['lj'] = '<esc>',
        ['<C-h>'] = '<esc><C-w>h',
        ['<C-j>'] = '<esc><C-w>j',
        ['<C-k>'] = '<esc><C-w>k',
        ['<C-l>'] = '<esc><C-w>l',
        [']p'] = 'dp',
        ['[p'] = 'dkkp',
        ['s('] = 'di()<esc>P',
        ['s['] = 'di[]<esc>P',
        ['s{'] = 'di{}<esc>P',
        ['s<'] = 'di<><esc>P',
        ['s"'] = 'di""<esc>P',
        ["s'"] = "di''<esc>P",
    },
}

------------------------------------------------------------------------------------------
-- plugins {{{1
------------------------------------------------------------------------------------------

util.load_plugins {
    -- autopairs    | automatically close braces {{{2
    autopairs = {
        'windwp/nvim-autopairs',
        requires = 'nvim-treesitter/nvim-treesitter'
    },
    -- bufferline   | 'tab' display at the top {{{2
    bufferline = {
        'akinsho/nvim-bufferline.lua',
        requires = 'kyazdani42/nvim-web-devicons',
    },
    -- colorizer    | visualize colorcodes {{{2
    colorizer = {
        'norcalli/nvim-colorizer.lua',
    },
    -- comment      | toggle comments w treesitter {{{2
    comment = {
        'numToStr/Comment.nvim'
    },
    -- cmp          | autocompletion {{{2
    cmp = {
        'hrsh7th/nvim-cmp',
        after = {
            'nvim-autopairs',
            'LuaSnip',
        },
        requires = {
            { 'hrsh7th/cmp-buffer',                  after = 'nvim-cmp' },
            { 'saadparwaiz1/cmp_luasnip',            after = 'nvim-cmp' },
            { 'hrsh7th/cmp-nvim-lua',                after = 'nvim-cmp' },
            { 'hrsh7th/cmp-path',                    after = 'nvim-cmp' },
            { 'hrsh7th/cmp-nvim-lsp',                after = 'nvim-cmp' },
            { 'hrsh7th/cmp-nvim-lsp-signature-help', after = 'nvim-cmp' },
            'onsails/lspkind.nvim',
        },
    },
    -- crates       | rust package autocomplete source {{{2
    crates = {
        'saecki/crates.nvim',
        event = { 'BufRead Cargo.toml' },
        after = { 'nvim-cmp' },
        requires = { 'nvim-lua/plenary.nvim' },
    },
    -- dap          | debugger integration {{{2
    dap = {
        'mfussenegger/nvim-dap',
        keys = { '<leader>b', '<leader>B' },
    },
    -- easy-align   | text alignment {{{2
    easy_align = {
        'junegunn/vim-easy-align',
    },
    -- gitsigns     | git diff, blame, etc. {{{2
    gitsigns = {
        'lewis6991/gitsigns.nvim',
        requires = { 'nvim-lua/plenary.nvim' }
    },
    -- indent-bl    | show divider for indents {{{2
    indent_blankline = {
        'lukas-reineke/indent-blankline.nvim',
    },
    -- lspconfig    | lsp config helper {{{2
    lspconfig = {
        'neovim/nvim-lspconfig',
        after = 'cmp-nvim-lsp',
    },
    -- lualine      | statusline {{{2
    lualine = {
        'hoob3rt/lualine.nvim',
        requires = {
            'kyazdani42/nvim-web-devicons',
            { 'arkav/lualine-lsp-progress', after = 'lualine.nvim' }
        },
    },
    -- CONFIG ME luasnip      | snippets {{{2
    luasnip = {
        'L3MON4D3/LuaSnip',
    },
    -- neorg        | note taking {{{2
    neorg = {
        'nvim-neorg/neorg',
        after = 'nvim-treesitter',
        ft = 'norg',
        requires = 'nvim-lua/plenary.nvim',
    },
    -- nix          | nix language support {{{2
    nix = {
        'LnL7/vim-nix',
    },
    -- packer       | plugin manager {{{2
    packer = {
        'wbthomason/packer.nvim',
        requires = { 'nvim-lua/plenary.nvim' },
    },
    -- presence     | discord rich presence because why the fuck not {{{2
    presence = {
        'andweeb/presence.nvim',
    },
    -- splitjoin    | split list into multiple lines {{{2
    splitjoin = {
        -- gS and gJ
        'AndrewRadev/splitjoin.vim',
    },
    -- telescope    | fuzzy finder {{{2
    telescope = {
        'nvim-telescope/telescope.nvim',
        requires = {
            'nvim-lua/popup.nvim',
            'nvim-lua/plenary.nvim',
            'nvim-telescope/telescope-ui-select.nvim',
            { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make', },
        },
    },
    -- treesitter   | parser tool for highlighting, braces, etc. {{{2
    treesitter = {
        'nvim-treesitter/nvim-treesitter',
        branch = 'master',
        run = ':TSUpdate',
    },
    -- virt-column  | print character as colorcolumn {{{2
    virt_column = {
        'lukas-reineke/virt-column.nvim',
    },
    -- }}}2
}

------------------------------------------------------------------------------------------
-- autocommands {{{1
------------------------------------------------------------------------------------------

local opt_local = vim.opt_local

util.autocmd.augroup('user') {
    BufEnter = {
        -- FIXME
        ['term://*'] = ':startinsert',
    },
    BufNew = {
        -- FIXME
        ['term://*'] = ':startinsert',
    },
    BufReadPost = {
        ['*'] = util.view.loadview,
    },
    BufWritePre = {
        ['*'] = [[:%s/\s\+$//e]],
    },
    BufWritePost = {
        ['*'] = util.view.mkview,
    },
    FileType = {
        ['help'] = function ()
	        opt_local.colorcolumn = ''
            opt_local.cursorline = false
            opt_local.foldcolumn = '9'
            opt_local.scrolloff = 99
            opt_local.signcolumn = 'no'
            vim.cmd 'highlight clear FoldColumn'
        end,
        ['make'] = function()
            opt_local.expandtab = false
            opt_local.shiftwidth = 8
            opt_local.softtabstop = 8
            opt_local.tabstop = 8
            opt_local.list = true
        end,
        [{ 'css', 'html', 'javascript', 'nix', 'xml', 'yaml' }] = function()
            opt_local.expandtab = true
            opt_local.shiftwidth = 2
            opt_local.softtabstop = 2
            opt_local.tabstop = 2
        end,
    },
    TextYankPost = {
        -- lambda forces 0 argument call
        ['*'] = function() vim.highlight.on_yank() end,
    },
}

------------------------------------------------------------------------------------------
-- }}}1
------------------------------------------------------------------------------------------

-- vim: foldmethod=marker
