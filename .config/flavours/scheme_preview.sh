#!/bin/bash

readonly reset="\e[0m"

function help() {
    echo usage:
    echo "$0 [-v|--verbose] path-to-scheme.yaml"
}

function as_hex() {
    printf '%X' "$1"
}

function as_dec() {
    echo $(( 16#$1 ))
}

function hex_to_ansi() {
    # input: something like 22aaff
    # output: something like 34;170;255
    r=$(as_dec "${1:0:2}")
    g=$(as_dec "${1:2:2}")
    b=$(as_dec "${1:4:2}")
    echo "\e[0;48;2;${r};${g};${b}m"
}

function read_scheme() {
    local -rn scheme_info=$1
    local -r scheme_file=$2
    local -r color_re="base0(.): \"(.*)\""

    scheme_info=()
    while read -r line; do
        if [[ "$line" =~ $color_re ]]; then
            # shcheck doesn't understand indirection
            # shellcheck disable=SC2034
            scheme_info[$(as_dec "${BASH_REMATCH[1]}")]="${BASH_REMATCH[2]}"
        fi
    done < "$scheme_file"
}

function print() {
    local -n scheme_data=$1
    for i in {0..7}; do
        shade_fmt=$(hex_to_ansi "${scheme_data[i]}")
        shade_line+="$shade_fmt  "
        color_fmt=$(hex_to_ansi "${scheme_data[i + 8]}")
        color_line+="$color_fmt  "
    done
    echo -e "${shade_line}${reset}"
    echo -e "${color_line}${reset}"
}

function verbose_print() {
    local -n scheme_data=$1
    for i in {0..7}; do
        local left_hex=${scheme_data[i]}
        local left_color
        left_color=$(hex_to_ansi "$left_hex")
        echo -ne "$left_color     $reset  base0${i}: $left_hex \t"

        local right_hex=${scheme_data[i + 8]}
        local right_color
        right_color=$(hex_to_ansi "$right_hex")
        echo -e "$right_color     $reset  base0$(as_hex $(( i + 8)) ): $right_hex"
    done
}

for opt in "$@"; do
    case $opt in
        --verbose) ;&
        -v)
            verbose=yes
            ;;
        -*)
            help
            exit
            ;;
        *)
            if [ -e "$opt" ]; then
                scheme_path="$opt"
            else
                echo "$0: no such file: $opt"
                exit
            fi
            ;;
    esac
done

if [ -z "$scheme_path" ]; then
    help
    exit
fi

# shcheck doesn't understand indirection
# shellcheck disable=SC2034
declare -a scheme
read_scheme scheme "$scheme_path"

if [[ "$verbose" == yes ]]; then
    verbose_print scheme
else
    print scheme
fi
