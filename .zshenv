# top level zshenv (main one's in $ZDOTDIR)
# symlink me to /root/!

if [[ -z "$XDG_CONFIG_HOME" ]]; then
    if (( UID + EUID )); then
        export XDG_CONFIG_HOME="$HOME"/.config
    else
        export XDG_CONFIG_HOME=/home/$(logname)/.config
    fi
fi

if [[ -d "$XDG_CONFIG_HOME/zsh" ]]; then
    export ZDOTDIR="$XDG_CONFIG_HOME"/zsh
fi

source "$ZDOTDIR"/.zshenv
